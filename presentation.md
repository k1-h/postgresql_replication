# PostgreSQL Replication & High Availability

**Keyvan Hedayati**



## What I'm gonna talk about?
* Understanding Replication <!-- .element: class="fragment" data-fragment-index="1" -->
* PostgreSQL Transaction Log <!-- .element: class="fragment" data-fragment-index="2" -->
* PITR & Warm Standby  <!-- .element: class="fragment" data-fragment-index="3" -->
* Asynchronous & Synchronous Replication <!-- .element: class="fragment" data-fragment-index="4" -->
* BDR & PGPool-II & Other solutions <!-- .element: class="fragment" data-fragment-index="5" -->
* Failover & High Availability <!-- .element: class="fragment" data-fragment-index="6" -->
* Sharding <!-- .element: class="fragment" data-fragment-index="7" -->



# What is replication anyway?
<!-- Let's talk about some boring but necessary concepts -->


## Definition
Copying data frequently from a database in one server to a database in
another server, to achieve:
* Reliability
* Fault-Tolerance
* Accessibility


## Types of replication
 * Single-Master vs Multi-Master
 * Synchronous vs Asynchronous
 * Logical vs Physical
 * Streaming replication vs Log Shipping
 * Hot Standby vs Warm Standby

Note:
Some of these are PostgreSQL specific


## Single vs Multi Master
![](imgs/master_to_master.jpg)


## Sync vs Async

| Type |  Write request blocked until | Lag | Data Loss |
| -- | -- | -- |
| Sync | All nodes or quorum acknowledge receiving data | No | No |
| Async | Only first node acknowledge receiving data | Yes | Yes |


### Sync vs Async: Write request block
Pay attention to numbers
![](imgs/SyncVsAsync-Replication.png)


### Sync vs Async: Lag

![](imgs/async_delay.jpg)


## Physical vs Logical Replication
* Physical replication means that the system will move data as is to the
remote box. So, if something is inserted, the remote box will get data
in binary format, not via SQL.
* Logical replication means that a change, which is equivalent to data
coming in, is replicated.


### Physical Replication
* Sends nearly disk block changes
* Sends everything:
 * All tables in every database
 * Index entries
 * VACUUM
 * Hint bits
* Sends A lot of excess data


### Logical Replication
* Sends only:
 * One database at a time
 * Row changes
 * For committed transactions
* Conflicts won't happen
* Replication across major versions and architectures and OSes
* Aborted transactions writes are never replicated at all
* Allows decoupling of the way data is stored from the way it is transported and replicated


### Logical Replication (Cons)
* Restrictions on supported DDL
* Apply process is more complicated than physical replication and requires more resources
* It replays transactions in commit order
 * Small fast transactions can get stuck behind a big transaction
* So there can be a long delay between a big transaction committing on the master and being applied to the replica.
* Big transactions may have longer replication delays because replication doesn't start until the transaction completes


## Hot vs Warm Standby
* In warm standby server won't accept queries
* In hot standby server will accept read-only queries
* Not even temporary tables may be written



## Backbone of Replication:
# Write-Ahead Log
Akka: WAL, Transaction Log, XLOG


## What is Write-Ahead Log?
* All modifications are written to a log before they are applied
* Technique for providing Atomicity and Durability in ACID
 * Atomicity: Each transaction must be "all or nothing
 * Durability: Ensures a committed transaction survives crash and power loss


### Example
Suppose we are doing a transaction and system *crashes in the middle of* write


### Without WAL
* We were writing directly to data files, we don't know:
 * Is written data correct or corrupted?
 * Where is the written data?
 * Is transaction succeeded or failed?
 * Was there even a transaction?


### With WAL
* First we write to log files, then data files:
 * Each WAL entry has a checksum and a sequence number
 * If writing to WAL fails we can simply find out with invalid WAL segment
 * If writing to data files fails we can replay WAL and finish or rollback transactions


## WAL in PostgreSQL
* It is in segments


## Interested?
A lot to be said, but there is no time.

More resources:
* [The Internals of PostgreSQL : Chapter 9: WAL](http://www.interdb.jp/pg/pgsql09.html)
* [Welcome to Write-Ahead Log](https://wiki.postgresql.org/images/d/da/PGDay2009-WAL.pdf)
* [WAL Internals Of PostgreSQL](https://www.pgcon.org/2012/schedule/attachments/258_212_Internals%20Of%20PostgreSQL%20Wal.pdf)



## Enough with the Theory!
![](imgs/book.webp)   <!-- .element: style="margin: 0; border: 0; box-shadow: none; background: none" -->



# Asynchronous Replication



# Log Shipping


**How it works?**
![](imgs/wam_standby.png)


## More Details

* Basically copying WAL files to another database server
* Copy is asynchronous and servers are loosely coupled
* You can transport logs using anything: NFS, FTP, rsync or cloud storage
* No direct network connection is required
* Replication can have long delays
* Data can be lost depending on transaction rate and configurations

Note:
When there is not many transactions wal files won't be created so it
won't be shipped


## How it's done? (Master)
* Master `postgresql.conf`:
 * wal_level = replica
 * archive_mode = on
 * archive_command = 'cp %p /archive/%f'
* Modify `pg_hba.conf` to allow replication on master

Note:
In wal_level=minimal, there are some optimizations in PostgreSQL that
allow XLOG writing to be skipped.


## How it's done? (Slave)
* Use `pg_basebackup` in order to get master data
* Add `recovery.conf` in destination directory:
 * restore_command = 'cp /archive/%f %p'
 * archive_cleanup_command = 'pg_archivecleanup /archive %r'
* Slave `postgresql.conf`:
 * hot_standby = on
* Start slave PostgreSQL and it will start consuming WAL files

Note:
Database can not be used in warm standby mode


## Other usages
* Point in time Recovery
* Delayed backup
* Creating a history of all changes



# Streaming replication


**How it works?**

![](imgs/streaming_replication.png)


## More Details
* Each change is sent to replica server directly over network as it happens
* Has little or no delay
* There is a possibility for query conflicts
* You can/should use it with log shipping or replication slots
* You can use it achieve cascaded replication


## Replication Slots
* Allows connection to the transaction log stream in a standard way and consume data
* Make sure that the master deletes XLOG only when it has been safely
  consumed by the replica
* It's very convenient and easier to use than log shipping
* If not consumed it will fill disk space


## How it's done? (Master)
* Master `postgresql.conf`:
 * wal_level = replica
 * max_wal_senders = 2
 * max_replication_slots = 2
* Modify `pg_hba.conf` to allow replication on master
* Run following query: SELECT * FROM pg_create_physical_replication_slot('repl_slot');


## How it's done? (Slave)
* Use `pg_basebackup` in order to get master data
* Add `recovery.conf` in destination directory:
 * standby_mode = on
 * primary_conninfo= ' host=sample.postgresql-support.de port=5432 '
 * primary_slot_name = 'repl_slot'
* Slave `postgresql.conf`:
 * hot_standby = on
* Start slave PostgreSQL and it will start consuming WAL files


## Cascaded Replication
![](imgs/Cascaded_Replication.png)


## Query Conflicts
![](imgs/conflict.png)


## Query Conflicts
* You can change configuration with these options:
 * max_standby_archive_delay = 30s
 * max_standby_streaming_delay = 30s


## Promoting Slave To Master
* This will change WAL timeline
* Make sure you stop master beforehand
* pg_ctl promote
* trigger_file in recovery.conf



# Synchronous Replication


## Pros
* No data will be lost if master crashes
* There is no delay between master and slave
 * So query result always is same on allow servers


## Cons
* It is expensive
 * Sending data through network is slow
 * Writing data to disk is slow
* If slave fails master will fail


## Compromise
* You can compromise between speed and consistency with
  synchronous_commit:
 * on: Waits until both servers report that the data has been written
   safely
 * remote_write: Remote server has accepted the XLOG and passed it on
   to the operating system without flushing things to the disk
 * local: Basically asynchronous replication


## PostgreSQL is better than that!
* You can change synchronous_commit in transaction level
* Disable synchronous replication for non critical queries:
``` plsql
BEGIN;
CREATE TABLE t_test (id int4);
SET synchronous_commit TO local;
```


## How it's done?
* Simple, just setup asynchronous replication
* Master `postgresql.conf`:
 * synchronous_standby_names = 'application_name'
* Use specified application_name when connecting to database name



### Master to Master Replication
# BDR



## Details
* Uses asynchronous logical replication
*

# Other solutions

* Shared Disk Failover
* PGPool-II
* File System Replication (DRBD)
* Slony



# Failover & High Availability


## Repmgr


## Pacemaker and friends


## Ptroni



# Another method for scaling: Sharding



### Interested?
## Here is the source
![](imgs/cover.png) <!-- .element: style="margin: 0;" -->



# Thanks

* Twitter: [@K1Hedayati](https://twitter.com/K1Hedayati)
* Personal Page: [k1h.ir](http://k1h.ir/)
* Slides: [k1h.ir/postgresql_replication](http://k1h.ir/postgresql_replication/)
